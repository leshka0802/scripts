from openpyxl.styles import PatternFill
from openpyxl.comments import Comment

from report.base import (
    Report, DecimalColumn, StringColumn, DateStartCol,
    AggregateValCol, AggregateByKeysColumn)


def get_development_status(cell):
    """
    Разбор значения ячейки
    :param cell:
    :return:
    """
    if 'Работа над запросом' in cell.value or cell.value.startswith(
            'Разработка'):
        name = 'Разработка'
    elif cell.value.startswith('Анализ'):
        name = 'Анализ'
    elif cell.value.startswith('Администрирование'):
        name = 'Администрирование'
    elif cell.value.startswith('Подготовка данных'):
        name = 'Подготовка данных'
    else:
        name = cell.value

    return name


class ActivityCol(StringColumn):
    """
    Колонка узкого назначения.
    Возвращает вид деятельности по задаче
    """

    activity_dict = {
        'Разработка': get_development_status,
    }

    def group_key(self, row):
        # 25: Колонка "Вид работ"
        val = row[25].value
        method = self.activity_dict.get(val)
        if method:
            return method(row[self.cell_source_id])
        else:
            return val

    def extract(self, row, data):
        # 25: Колонка "Вид работ"
        val = row[25].value
        method = self.activity_dict.get(val)
        if method:
            return method(row[self.cell_source_id])
        else:
            return val


class PowerColumn(DecimalColumn):
    """
    Колонка узкого назначения.
    Выводит отношение
    суммы оценочных часов по закрытым задачам
    к затраченному времени по всем задачам
    """
    def extract(self, row, data):
        value = None
        estimation = data['Закрыто часов']
        spent = data['Затраченное время']
        if spent:
            value = estimation / spent

        value = self.to_python(value)
        return value


class EstimationColumn(AggregateByKeysColumn):
    """
    Колонка узкого назначения.
    Выводит сумму оценочных часов по задачам
    """
    # индекс колонки "Ключ запроса"
    key_index = (0,)


class ClosedTaskColumn(AggregateByKeysColumn):
    """
    Колонка узкого назначения.
    Выводит сумму оценочных часов по закрытым задачам в разрезе пользователей
    """
    # индексы колонок "Ключ запроса", "Имя пользователя", "Статус запроса"
    key_index = (0,)

    def extract(self, row, data):
        val = self.to_python(data.get(self.new))
        status = data['Статус запроса']
        if status == 'Закрыт':
            val = super(ClosedTaskColumn, self).extract(row, data)

        return val


class JiraReport(Report):

    # ссылки для прямого обращения к колонками
    estimation_column = EstimationColumn(old='Первоначальная оценка запроса')
    key_column = StringColumn(old='Ключ запроса', new='Номер задачи Jira')

    all_columns = [
        StringColumn(old='Полное имя', new='ФИО сотрудника'),
        key_column,
        StringColumn(old='Тема запроса'),
        DateStartCol(old='Дата работы'),
        estimation_column,
        StringColumn(old='Статус запроса'),
        ActivityCol(old='Описание работы'),
        ClosedTaskColumn(
            old='Первоначальная оценка запроса', new='Закрыто часов'),
        AggregateValCol(old='Часов', new='Затраченное время'),
        PowerColumn(old='Отношение оценки к затратам'),
    ]

    def __init__(self, path, defect_tasks):
        super(JiraReport, self).__init__(path)
        # словарь вида: {task_id:
        #                   {'origin': None, 'modified': None}}
        # где origin - изначальное время задачи,
        # modified - измененное время задачи
        self.defect_tasks = dict(map(
            lambda x: (x[0],
                       {'origin': None, 'modified': x[1]}),
            defect_tasks.items()))

    def prepare(self):
        super(JiraReport, self).prepare()

        for row in self.main_ws.iter_rows(min_row=2):
            # получение инстансов ячеек
            cell_task = row[self.key_column.cell_source_id]
            cell_estimation = row[self.estimation_column.cell_source_id]

            val = self.defect_tasks.get(cell_task.value, {}).get('modified')
            if val is not None:
                self.defect_tasks[
                    cell_task.value]['origin'] = cell_estimation.value
                cell_estimation.value = val

    def cell_formatting(self, cell, column):
        super().cell_formatting(cell, column)

        if cell.value in self.defect_tasks:
            origin_val = self.defect_tasks[cell.value]['origin']
            cell.comment = Comment(
                f'У задачи изменено значение колонки '
                f'"Первоначальная оценка запроса". '
                f'Изначальное значение: {origin_val}', 'python scripts')

            gray_fill = PatternFill(
                start_color='cccccc', fill_type='solid')
            cell.fill = gray_fill
