import decimal
from collections import defaultdict
from collections.abc import Iterable

from openpyxl import load_workbook
from openpyxl.styles import Alignment


# region Классы для работы с колонками

class BaseColumn(object):

    def __init__(self, old, new=None):
        self.cell_source_id = None
        "индекс колонки в исходном отчете"

        self.old = old
        "наименование колонки в исходном отчете"

        self.new = new or old
        "наименование колонки в новом отчете"

    def extract(self, row, data):
        """
        Извлечь значение ячейки
        :param row:
        :param data: Словарь значений эквивалентный одной строке нового отчета
                     ключи словаря имена колонок нового отчета
        :type data: dict
        :return:
        """
        value = row[self.cell_source_id].value
        value = self.to_python(value)
        return value

    @staticmethod
    def to_python(value):
        return value

    def group_key(self, row):
        """
        Получить имя поля для группировки
        :param row:
        :return:
        """
        return row[self.cell_source_id].value


class StringColumn(BaseColumn):

    @staticmethod
    def to_python(value):
        if value is None:
            value = ''

        return str(value)


class DecimalColumn(BaseColumn):

    @staticmethod
    def to_python(value):
        if value is None:
            value = 0

        return round(decimal.Decimal(value), 2)


class AggregateValCol(DecimalColumn):
    """
    Суммирование значений ячеек по строкам
    """

    def extract(self, row, data):
        val = super().extract(row, data)
        return data.get(self.new, decimal.Decimal(0)) + val


class ConcatValCol(StringColumn):

    def __init__(self, sep='\n', *args, **kwargs):
        super(ConcatValCol, self).__init__(*args, **kwargs)
        self.sep = sep

    def extract(self, row, data):
        val = super(ConcatValCol, self).extract(row, data)
        return data.get(self.new, '') + self.sep + val


class DateStartCol(BaseColumn):

    def extract(self, row, data):
        val = super(DateStartCol, self).extract(row, data)
        old_val = data.get(self.new)
        if old_val and old_val <= val:
            return old_val
        return val


class AggregateByKeysColumn(AggregateValCol):
    """
    Колонка агрегирует значения,
    группируя их по значениям ключей колонок, указанных в KEY_INDEX
    """
    key_index = None
    """
    Индексы колонок, значения которых являются ключами для группировки
    """

    cache_key = '_cache'

    def __init__(self, *args, **kwargs):
        super(AggregateByKeysColumn, self).__init__(*args, **kwargs)

        # для исключения пересечений кэшей разных колонок
        self.cache_key = f'{self.__class__.__name__}{self.cache_key}'

    def get_key(self, row):
        assert isinstance(self.key_index, Iterable)
        return [row[index].value for index in self.key_index]

    def extract(self, row, data):
        """
        В data создается елемент _cache в качестве кэша,
        для хранения уже учтенных значений из списка индексов key_index
        :param row:
        :param data:
        :return:
        """

        if self.cache_key not in data:
            data[self.cache_key] = []

        val = data.get(self.new, decimal.Decimal(0))
        key = self.get_key(row)
        if key not in data[self.cache_key]:
            data[self.cache_key].append(key)
            val = super().extract(row, data)

        return val

# endregion


class Report(object):

    all_columns = None
    "Список всех доступных колонок отчета"

    columns = None
    "Список колонок использумых при построении листа(worksheet)"

    group_by = None
    "Список наименований колонок для группировки"

    group_columns = None
    "Отсоритрованный список колонок по group_by"

    plain_data = None
    "Линейный список значений из tree_data"

    tree_data = None
    "Дерево записей, узлы - поля для группировки"

    def __init__(self, path):
        """
        :param path: str:
        """
        assert self.all_columns
        try:
            self.main_wb = load_workbook(path)
        except FileNotFoundError as e:
            exit(e)

        self.main_ws = self.main_wb.active

    def create_sheet(self, wb, sheet_name, group_by, columns):
        """
        :param wb:
        :type wb: openpyxl.Workbook

        :param sheet_name: str:

        :param group_by: list: Список наиманование колонок для группировки

        :param columns: list: Список наименований колонок нового отчета
        использумых при построении листа(worksheet)
        """
        self.group_by = group_by or []

        self.group_columns = []

        self.tree_data = defaultdict(dict)

        self.columns = [
            col for col in self.all_columns for name in columns
            if col.new == name]

        # Соответсвие колонок columns с колоноками отчета
        self.prepare()

        self.build_tree()
        self.to_plain()

        ws = wb.create_sheet(sheet_name)
        ws.set_printer_settings(
            paper_size=ws.PAPERSIZE_A4, orientation='landscape')
        self.bind_data(ws)

    def prepare(self):
        """
        Создание соответствия поля из self.columns
        и индекса колонки из базовой таблицы
        """

        title_row = None
        for row in self.main_ws.iter_rows(max_row=1):
            title_row = row

        for col in self.all_columns:
            for cell_id, cell, in enumerate(title_row):
                if cell.value == col.old:
                    col.cell_source_id = cell_id

                    if cell.value in self.group_by:
                        self.group_columns.insert(
                            self.group_by.index(cell.value),
                            col
                        )
                    break

    def build_tree(self):
        """
        1. Группировка
        2. Извлечение значений для отчета по self.columns
        """
        for row in self.main_ws.iter_rows(min_row=2):
            el_dict = self.tree_data
            for col in self.group_columns:
                el_dict = el_dict.setdefault(
                    col.group_key(row), {})
            for col in self.all_columns:
                el_dict[col.new] = col.extract(row, el_dict)

    def to_plain(self):
        """
        Линейное представление сгруппированных записей
        """
        def plain(data, deep=0):
            result = []
            if deep == len(self.group_by):
                result.append(data)
            else:
                for v in data.values():
                    result.extend(plain(v, deep + 1))

            return result

        self.plain_data = plain(self.tree_data)

    def bind_data(self, ws):
        """
        Создание нового отчета

        :param ws:
        :type ws: openpyxl.worksheet.worksheet.Worksheet
        """
        for idx, col in enumerate(self.columns, start=1):
            cell = ws.cell(1, idx)
            cell.value = col.new

        for row_id, col_data in enumerate(self.plain_data, start=2):
            ws.row_dimensions[row_id].height = 40
            for idx, col in enumerate(self.columns, start=1):
                cell = ws.cell(
                    row=row_id,
                    column=idx,
                    value=col_data[col.new]
                )
                self.cell_formatting(cell, col)

    def cell_formatting(self, cell, column):
        """
        Форматирование ячейки

        :param cell:
        :type cell: openpyxl.cell.cell.Cell

        :param column:
        :type column: BaseColumn
        """
        cell.alignment = Alignment(wrapText=True, horizontal='center')
        cell.parent.column_dimensions[cell.column].width = 25

