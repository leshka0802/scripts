import argparse
from datetime import date

from openpyxl import Workbook

from report.jira import JiraReport
from report.redmine import RedmineReport


def main():
    parser = argparse.ArgumentParser(
        description='python3.6 __main__.py '
                    ' --jira_file <path/to/jira_file.xlsx>'
                    ' --redmine_file <path/to/redmine_file.xlsx>'
    )

    parser.add_argument(
        '--jira_file', type=str,
        help='Путь до файла отчета из jira с раширением .xlsx')

    parser.add_argument(
        '--redmine_file', type=str,
        help='Путь до файла отчета из Redmine с раширением .xlsx')
    parser.add_argument(
        '--defect_tasks', type=str, action='append', default=[],
        help='ID задачи с измененным оценочным временем. '
             'example: --defect_tasks BOZIK-<#####>=0')
    args = parser.parse_args()

    if not args.jira_file and not args.redmine_file:
        exit('Path to "jira_file" or "redmine_file" is required')

    wb = Workbook()
    wb._sheets = []
    defect_tasks = dict(map(lambda x: x.split('='), args.defect_tasks))

    if args.jira_file:
        jira_report = JiraReport(
            path=args.jira_file,
            defect_tasks=defect_tasks)
        jira_report.create_sheet(
            wb=wb,
            sheet_name='Jira отчет',
            group_by=['Ключ запроса', 'Полное имя'],
            columns=[
                'ФИО сотрудника',
                'Номер задачи Jira',
                'Тема запроса',
                'Дата работы',
                'Первоначальная оценка запроса',
                'Затраченное время',
                'Статус запроса',
            ]
        )

        jira_report.create_sheet(
            wb=wb,
            sheet_name='Jira. По статусу запроса',
            group_by=['Полное имя', 'Статус запроса'],
            columns=[
                'ФИО сотрудника',
                'Статус запроса',
                'Первоначальная оценка запроса',
                'Затраченное время',
            ],
        )

        jira_report.create_sheet(
            wb=wb,
            sheet_name='Jira. Всего',
            group_by=['Полное имя'],
            columns=[
                'ФИО сотрудника',
                'Закрыто часов',
                'Затраченное время',
                'Отношение оценки к затратам'
            ]
        )

    if args.redmine_file:
        redmine_report = RedmineReport(path=args.redmine_file)
        redmine_report.create_sheet(
            wb=wb,
            sheet_name='Redmine отчет',
            group_by=['Задача', 'Пользователь'],
            columns=[
                'ФИО сотрудника',
                'Задача',
                'Статус',
                'Дата начала работы',
                'Оценка разработки',
                'Затраченное время',
            ]
        )

        redmine_report.create_sheet(
            wb=wb,
            sheet_name='Redmine. По статусу запроса',
            group_by=['Пользователь', 'Статус'],
            columns=[
                'ФИО сотрудника',
                'Статус',
                'Оценка разработки',
                'Затраченное время',
                'Отношение оценки к затратам'
            ],
        )

        redmine_report.create_sheet(
            wb=wb,
            sheet_name='Redmine. По видам деятельности',
            group_by=['Пользователь', 'Деятельность'],
            columns=[
                'Пользователь',
                'Деятельность',
                'Затраченное время',
            ],
        )

        redmine_report.create_sheet(
            wb=wb,
            sheet_name='Redmine. Всего',
            group_by=['Пользователь'],
            columns=[
                'ФИО сотрудника',
                'Затраченное время',
            ]
        )

    wb.save('{}.xlsx'.format(date.today().strftime('%d.%m.%Y')))


if __name__ == '__main__':
    main()
