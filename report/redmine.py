from report.base import (
    Report, StringColumn, DateStartCol, AggregateValCol,
    AggregateByKeysColumn, DecimalColumn)


class PowerColumn(DecimalColumn):

    def extract(self, row, data):
        value = None
        estimation = data['Оценка разработки']
        spent = data['Затраченное время']
        if spent:
            value = estimation / spent

        value = self.to_python(value)
        return value


class EstimationColumn(AggregateByKeysColumn):

    # индекс колонки "Задача"
    key_index = (5,)


class RedmineReport(Report):

    all_columns = [
        StringColumn(old='Проект'),
        StringColumn(old='Задача'),
        StringColumn(old='Пользователь', new='ФИО сотрудника'),
        StringColumn(old='Статус'),
        DateStartCol(old='Дата', new='Дата начала работы'),
        EstimationColumn(old='Оценка разработки'),
        AggregateValCol(old='час(а,ов)', new='Затраченное время'),
        StringColumn(old='Комментарий'),
        StringColumn(old='Деятельность'),
        PowerColumn(old='Отношение оценки к затратам')
    ]
