### Description

Скрипт позволяет конфигурировать(группировать) выгруженные отчеты 
по трудозатратам из Jira/Redmine

**Важно:**
Выгруженные отчеты из Jira/Redmine нужно пересохранить с расширением .xlsx
(openpyxl с xls/csv не работает)

### Build

```
python setup.py sdist --formats gztar --dist-dir ./
```

### Installing

```
virtualenv --python=python3.6 venv
source venv/bin/activate
pip install scripts-<version>.tar.gz
```

### Usage

```
source /path/to/venv/bin/activate
report  --jira_file path/to/jira_file.xlsx --redmine_file path/to/redmine_file.xlsx
```

### Example from source

```
git clone git@bitbucket.org:leshka0802/scripts.git
virtualenv --python=python3.6 venv
source venv/bin/activate
cd scripts
pip install -e ./
report  --redmine_file example/redmine.xlsx
```
    
