from pip._internal.req import parse_requirements
from pip._internal.download import PipSession
from setuptools import setup, find_packages

REQUIREMENTS = list(map(
    lambda x: str(x.req),
    parse_requirements(
        'requirements', session=PipSession())))

setup(
    name='scripts',
    version='0.1',
    install_requires=REQUIREMENTS,
    include_package_data=True,
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'report = report.__main__:main',
        ]
    },
)
